import textract

# Extract p393-400 as text
# text = textract.process('LEGITEXT000006072665-393-400.pdf')
# with open('csp-393-400.txt', 'wb') as f:
#     f.write(text)

# Extract loi sante 2019
text = textract.process('Loi Santé 2019 V1.pdf')
with open('loi-sante-2019.txt', 'wb') as f:
    f.write(text)
